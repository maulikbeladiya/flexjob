<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="index.php">
              <i class="mdi mdi-home menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="freelancer.php">
              <i class="mdi mdi-account menu-icon"></i>
              <span class="menu-title">Freelancer</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="client.php">
              <i class="mdi mdi-account-multiple menu-icon"></i>
              <span class="menu-title">Client</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="dashbord-category.php">
              <i class="mdi mdi-format-list-bulleted menu-icon"></i>
              <span class="menu-title">Category</span>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="dashbord-subcategory.php">
              <i class="mdi mdi-sitemap menu-icon"></i>
              <span class="menu-title">Sub Category</span>
            </a>
          </li> -->
          <!--<li class="nav-item">
            <a class="nav-link" href="dashbord-skill.php">
              <i class="mdi mdi-sunglasses menu-icon"></i>
              <span class="menu-title">Skill</span>
            </a>

            <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-circle-outline menu-icon"></i>
              <span class="menu-title">UI Elements</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Typography</a></li>
              </ul>
            </div>
          </li>
          </li>-->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#allProject" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-playlist-check menu-icon"></i>
              <span class="menu-title">All Project</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="allProject">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="job.php">All Project</a></li>
                <li class="nav-item"> <a class="nav-link" href="jobongoing.php">Ongoing Project</a></li>
                <li class="nav-item"> <a class="nav-link" href="jobcompleted.php">Completed Project</a></li>
                <li class="nav-item"> <a class="nav-link" href="jobcancle.php">Cancle Project</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="payment.php">
              <i class="mdi mdi-receipt menu-icon"></i>
              <span class="menu-title">Payment</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  data-toggle="collapse" href="#allReports" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-alert menu-icon"></i>
              <span class="menu-title">Reports</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="allReports">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="reportclient.php">Report Client</a></li>
                <li class="nav-item"> <a class="nav-link" href="reportfreelancer.php">Report Freelancer</a></li>
                <li class="nav-item"> <a class="nav-link" href="reportjob.php">Report Job</a></li>
              </ul>
            </div>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="mdi mdi-message-text menu-icon"></i>
              <span class="menu-title">Feedback</span>
            </a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="profile-setting.php">
              <i class="mdi mdi-settings menu-icon"></i>
              <span class="menu-title">Profile setting</span>
            </a>
          </li>
        </ul>
      </nav>