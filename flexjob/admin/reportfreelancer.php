<?php
  session_start();
  if (!isset($_SESSION["islogin"])) {
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>FlexJob Admin | Report freelancer</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/base/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  

  
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php require_once('partials/_navbar.php');?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

      <?php require_once('partials/_sidebar.php');?>

      <div class="main-panel">
        <div class="content-wrapper">
            <div class="col-md-12 stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">View User Reports</p>
                  <div class="table-responsive">
                    <table id="reports_data" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Id</th>
                            <th>On Report</th>
                            <th>Reason</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <?php require_once 'partials/_footer.php';?>
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="vendors/chart.js/Chart.min.js"></script>
  <script src="vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <script src="js/data-table.js"></script>
  <script src="js/jquery.dataTables.js"></script>
  <script src="js/dataTables.bootstrap4.js"></script>
  <script src="js/file-upload.js"></script>
  <!-- End custom js for this page-->
  

<script type="text/javascript">

 $(document).ready(function(){
  
  // MARK: fetchData from database for datatabels
  fetch_data();

  $('#reports_data tbody').on('click', 'button', function(){
    var name = $(this).attr("name");
    if (name == "deletereport") {
    	var id = $(this).attr("data-id");
      	if(confirm("Are you sure you want to delete this Report?")) {
        	$.ajax({
            	url:"actionFunction/report/update.php",
            	method:"POST",
            	data:{
              		id:id,
              		column_name: "status",
                  tblname: "userreport_tbl",
              		value: 1
            	},
            	success:function(data){
              		$('#reports_data').DataTable().destroy();
              		fetch_data();
                  alert(data);
            	}
        	});
      	}
    } else if (name == "deleterecord") {
     	var id = $(this).attr("data-id");
      	if(confirm("Are you sure delete this report From database?")) {
        	$.ajax({
            	url:"actionFunction/report/delete.php",
            	method:"POST",
            	data:{
              		id:id,
                  tblname:"userreport_tbl"
              	},
            	success:function(data){
              		$('#reports_data').DataTable().destroy();
              		fetch_data();
                  alert(data);
            	}
        	});
      	}
    } else if (name == "view") {
      var id = $(this).attr("data-id");
      myWindow = window.location.href = 'http://localhost/flexjob/admin/singlepage/usersingle.php?id='+id+'&report=on'; // myWindow = window.open('http://localhost/admin/singlepage/jobsingle.php?jid='+id, "_blank");
        myWindow.focus();
    }
  });

  	function fetch_data() {
    	var dataTable = $('#reports_data').DataTable({
    		"processing" : true,
    		"serverSide" : true,
    		"order" : [],
    		"ajax" : {
     			url:"actionFunction/report/fetch.php",
     			type:"POST",
     			data:{
              colid: "u_id",
        			tblName: "userreport_tbl"  
     			},
     			datatype:"json"
    		},
    		success:function(data) {
      			alert(data);
    		}
   		});
  	}
})
</script>
</body>

</html>