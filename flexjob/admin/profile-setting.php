<?php
	session_start();
	if (!isset($_SESSION["islogin"])) {
		header('Location: login.php');
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>FlexJob | Profile-settings</title>
	<!-- plugins:css -->
	<link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="vendors/base/vendor.bundle.base.css">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="css/style.css">
	<!-- endinject -->
	<link rel="shortcut icon" href="images/favicon.png" />
</head>
<body>
	<div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<?php require_once('partials/_navbar.php');?>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<!-- partial:partials/_sidebar.html -->
			<?php require_once('partials/_sidebar.php');?>
			<!-- partial -->
			<div class="main-panel">
				<div class="content-wrapper">	
					<div class="col-12 grid-margin stretch-card d-none d-md-flex">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Profiles</h4>
                  <div class="row">
                    <div class="col-3">
                      <ul class="nav nav-tabs nav-tabs-vertical" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="home-tab-vertical" data-toggle="tab" href="#home-2" role="tab" aria-controls="home-2" aria-selected="true">
                          <i class="mdi mdi-home-outline text-info ml-2"></i>
                          Home
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="profile-tab-vertical" data-toggle="tab" href="#profile-2" role="tab" aria-controls="profile-2" aria-selected="false">
                          <i class="mdi mdi-account-outline text-danger ml-2"></i>
                          Profile
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="passchange-tab-vertical" data-toggle="tab" href="#passchange-2" role="tab" aria-controls="passchange-2" aria-selected="false">
                          <i class="mdi mdi-account-key text-success ml-2"></i>
                          Change Password
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="contact-tab-vertical" data-toggle="tab" href="#contact-2" role="tab" aria-controls="contact-2" aria-selected="false">
                          <i class="mdi mdi-email-outline text-success ml-2"></i>
                          Reach
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-9">
                      <div class="tab-content tab-content-vertical">
                        <div class="tab-pane fade show active" id="home-2" role="tabpanel" aria-labelledby="home-tab-vertical">
                          <div class="text-center">
                            <img class="mb-3 w-25 rounded" src="images/upload/<?php echo $_SESSION['adminimg']; ?>" alt="sample image">
                            <h4 class="mt-0"><?php echo $_SESSION['uname'];?></h4>
                            <!-- <p class="mb-0"></p> -->

                            <center>
                              <div class="bottom border col-md-6" style="margin-bottom: 10px;"></div>
                            <div class="col-md-4">
                            <p class="clearfix">
                              <span class="float-left">
                                Role
                              </span>
                              <span class="float-right text-muted">
                                <a href="#">Admin Flexjob</a>
                              </span>
                            </p>
                            <p class="clearfix">
                              <span class="float-left">
                                Email
                              </span>
                              <span class="float-right text-muted">
                                <a href="#"><?php echo $_SESSION['uemail'];?></a>
                              </span>
                            </p>
                            <p class="clearfix">
                              <span class="float-left">
                                UserId
                              </span>
                              <span class="float-right text-muted">
                                <a href="#"><?php echo $_SESSION['userid'];?></a>
                              </span>
                            </p>
                            <p class="clearfix">
                              <span class="float-left">
                                Contact No
                              </span>
                              <span class="float-right text-muted">
                                <a href="#">+91 9998650030</a>
                              </span>
                            </p>
                            </div></center>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="profile-2" role="tabpanel" aria-labelledby="profile-tab-vertical">
                          <h4 class="card-title">Upload Profile Image</h4>
                          <form id="profileimage">
                            <div class="form-group">
                              <label>File upload</label>
                              <input type="file" name="proimg" id="proimg" class="file-upload-default">
                              <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                  <span class="input-group-append">
                                      <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                  </span>
                              </div>
                            </div>
                            <div class="form-group" id="imgpreview">
                                <!-- <img src="images/upload/admin.jpg" style="width: 100px; height: 100px;  border: 1px solid #ddd;  padding: 5px; object-fit: cover; border-radius: 4px;" alt="images" /> --> 
                            </div>
                            <input class="btn btn-primary" type="submit" id="uploadproimg" name="uploadproimg" value="Submit">
                          </form>
                        </div>
                        <div class="tab-pane fade" id="passchange-2" role="tabpanel" aria-labelledby="passchange-tab-vertical">
                          <h4 class="card-title">Change Password</h4>
                            <form id="resetpass" name="resetpass" action="#">
                              <fieldset>
                                  <div class="form-group">
                                    <label for="currentpass"> Current Password</label>
                                    <input id="currentpass" class="form-control" name="currentpass" minlength="2" type="Password" required>
                                  </div>
                                  <div class="form-group">
                                    <label for="newpass">New Password</label>
                                    <input id="newpass" class="form-control" type="Password" name="newpass" required>
                                  </div>
                                  <div class="form-group">
                                    <label for="confirmpass">Confirm Password</label>
                                    <input id="confirmpass" class="form-control" type="Password" name="confirmpass" required>
                                  </div>
                                  <input class="btn btn-primary" type="submit" id="changepass" name="changepass" value="Submit">
                              </fieldset>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="proinfo-2" role="tabpanel" aria-labelledby="passchange-tab-vertical">
                          <h4 class="card-title">Change Display Name</h4>
                            <form id="proinfo" name="proinfo" action="#">
                              <fieldset>
                                  <div class="form-group">
                                    <label for="name"> Change Display Name</label>
                                    <input id="name" class="form-control" name="name" minlength="2" type="text" required>
                                  </div>
                                  <div class="form-group">
                                    <label for="email">Change Email</label>
                                    <input id="email" class="form-control" type="text" name="email" required>
                                  </div>
                                  <input class="btn btn-primary" type="submit" id="changeinfo" name="changeinfo" value="Submit">
                              </fieldset>
                            </form>
                        </div>


                        <div class="tab-pane fade" id="contact-2" role="tabpanel" aria-labelledby="contact-tab-vertical">
                          <h4>Contact us </h4>
                          <p>
                            Feel free to contact us if you have any questions!
                          </p>
                          <p>
                            <i class="mdi mdi-phone text-info"></i>
                            +91 9998650030
                          </p>
                          <p>
                            <i class="mdi mdi-email-outline text-success"></i>
                            flexpro25@gmail.com
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
				</div>
				<!-- content-wrapper ends -->
				<!-- partial:partials/_footer.html -->
				<?php require_once 'partials/_footer.php';?>
				<!-- partial -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->

	<!-- plugins:js -->
	<script src="vendors/base/vendor.bundle.base.js"></script>
	<!-- endinject -->
	<!-- Plugin js for this page-->
	<script src="vendors/chart.js/Chart.min.js"></script>
	<script src="vendors/datatables.net/jquery.dataTables.js"></script>
	<script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
	<script src="vendors/jquery-validation/jquery.validate.min.js"></script>
	<!-- End plugin js for this page-->
	<!-- inject:js -->
	<script src="js/off-canvas.js"></script>
	<script src="js/hoverable-collapse.js"></script>
	<script src="js/template.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script src="js/dashboard.js"></script>
	<script src="js/data-table.js"></script>
	<script src="js/jquery.dataTables.js"></script>
	<script src="js/dataTables.bootstrap4.js"></script>
	<script src="js/tabs.js"></script>
	<script src="js/form-validation.js"></script>
	<script src="js/bt-maxLength.js"></script>
	<script src="js/file-upload.js"></script>
	<!-- End custom js for this page-->
	<script>
		
		$(document).ready(function(){

			$("#uploadproimg").click(function(event) {
    		event.preventDefault();
  		
				var file_data = $('#proimg').prop('files')[0];   
      			var form_data = new FormData();                  
      			form_data.append('file', file_data);
      			                           
      			$.ajax({
        			url: 'upload.php', // point to server-side PHP script 
        			//dataType: 'text',  // what to expect back from the PHP script, if anything
        			cache: false,
        			contentType: false,
        			processData: false,
        			data: form_data,                         
        			type: 'POST',
        			success: function(data){
            			 // $('#imgpreview').html('<img style="width: 100px; height: 100px;  border: 1px solid #ddd;  padding: 5px; object-fit: cover; border-radius: 4px;" src="'+ data + '" alt="images" />');
                  $('#imgpreview').html(data);
                  alert('sucessfully Profile image uploaded!!!');
                  window.location.reload();
        			}
      			});
      			
      		});

      $("#changepass").click(function(event) {
        event.preventDefault();
        var pass1 = $("#newpass").val();
        var oldpass = $("#currentpass").val();
        $.ajax({
          url: "passupdate.php",
          method: "POST",
          data: {
            curpass : oldpass,
            newpass: pass1
          },
          success: function(data) {
            if (data == 'true1234') {
              alert('Password sucessfully changed..!!!');  
              window.location.reload();
            } else {
              alert('Password not changed error :' + data);
            }
            
            // window.location = "login.php";
          }
        });
      });
  	});	

	</script>

</body>

</html>