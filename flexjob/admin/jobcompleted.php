<?php

  session_start();
  if (!isset($_SESSION["islogin"])) {
    header('Location: login.php');
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Admin-FlexJob | CompletedJob</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/base/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  

  
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php require_once('partials/_navbar.php');?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

      <?php require_once('partials/_sidebar.php');?>


      <div class="main-panel">
        <div class="content-wrapper">
            <div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel-2">Details of the Job</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body" id="modalbodyid">
                        </div>
                      </div>
                    </div>
            </div>
            <div class="col-md-12 stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">Completed Jobs</p>
                  <div class="table-responsive">
                    <table id="completedjob_data" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Job Name</th>
                            <th>Client</th>
                            <th>Hire Freelancer</th>
                            <th>View</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <?php require_once 'partials/_footer.php';?>
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <script src="js/data-table.js"></script>
  <script src="js/jquery.dataTables.js"></script>
  <script src="js/dataTables.bootstrap4.js"></script>
  
  <!-- End custom js for this page-->
  

<script type="text/javascript">

 $(document).ready(function(){
  
  // MARK: fetchData from database for datatabels
  fetch_data();

  $('#completedjob_data tbody').on('click', 'button', function(){
    var name = $(this).attr("name");
    if (name == "view") {
      var id = $(this).attr("data-id");
        myWindow = window.location.href = 'http://localhost/flexjob/admin/singlepage/jobsingle.php?jid='+id+'&jobcat=2';
        // myWindow = window.open('http://localhost/admin/singlepage/jobsingle.php?jid='+id, "_blank");
        myWindow.focus();  
    } else {
      alert(name); 
    }
  });

  function fetch_data() {
    var dataTable = $('#completedjob_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"actionFunction/completedjob/fetch.php",
     type:"POST",
     datatype:"json"
    },
    success:function(data) {
      alert(data);
    }
   });
  }
})
</script>
</body>

</html>