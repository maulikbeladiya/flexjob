<?php

  session_start();
  if (!isset($_SESSION["islogin"])) {
    header('Location: login.php');
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>FlexJob Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/base/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  

  
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php require_once('partials/_navbar.php');?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

      <?php require_once('partials/_sidebar.php');?>


      <div class="main-panel">
        <div class="content-wrapper">
            <div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel-2">Details of the Job</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body" id="modalbodyid">
                          <div class="col-12 grid-margin">
                            <div class="card">
                              <div class="card-body">
                                <h4 class="card-title">Update Category</h4>
                                <form class="form-sample" id="editcategoryform" action="#">
                                  <fieldset>
                                    <div class="row">
                                      <div class="col-md-5">
                                        <div class="form-group row">
                                          <div class="col-sm-10">
                                            <input type="text" id="editcatname" minlength="6" name="catname" class="form-control" placeholder="Category Name" required>
                                          </div>
                                        </div>
                                      </div>
                                      <button type="submit" id = "editCat" name = "editCat" class="btn btn-primary mb-4">Update category</button>
                                    </div>
                                  </fieldset>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
            </div>
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add Category</h4>
                  <form class="form-sample" id="categoryform" action="#">
                    <fieldset>
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group row">
                          <div class="col-sm-10">
                            <input type="text" id="catname" minlength="6" name="catname" class="form-control" placeholder="Category Name" required>
                          </div>
                        </div>
                      </div>
                      <button type="submit" id = "addCat" name = "addCat" class="btn btn-primary mb-4">Add category</button>
                    </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>      

            <div class="col-md-12 stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">All Category</p>
                  <div class="table-responsive">
                    <table id="category_data" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Id</th>
                            <th>Category Name</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <?php require_once 'partials/_footer.php';?>
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- <script src="vendors/chart.js/Chart.min.js"></script> -->
  <script src="vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <script src="js/data-table.js"></script>
  <script src="js/jquery.dataTables.js"></script>
  <script src="js/dataTables.bootstrap4.js"></script>
  <!-- <script src="js/file-upload.js"></script> -->
  <!-- End custom js for this page-->
  

<script type="text/javascript">

 $(document).ready(function(){
  
  // MARK: fetchData from database for datatabels
  fetch_data();

  // MARK: on click addCategoryButton
  $("#addCat").click(function(event) {

    event.preventDefault();
    var cat_name = $("#catname").val();
    if (cat_name != "" || !cat_name.isEmpty() || cat_name != null) {
    $.ajax({
      url: "actionFunction/category/addCategory.php",
      method: "POST",
      data: {
        category_name: cat_name
      },
      success: function(data) {
        alert(data);
      }
    })
    $('#category_data').DataTable().destroy();
    fetch_data();
  }
  })

  $('#category_data tbody').on('click', 'button', function(){
    var name = $(this).attr("name");
    if (name == "delete") {
      var id = $(this).attr("data-id");
      if(confirm("Are you sure you want to remove this?")) {
        $.ajax({
            url:"actionFunction/category/deleteCategory.php",
            method:"POST",
            data:{category_id:id},
            success:function(data){
              $('#category_data').DataTable().destroy();
              fetch_data();
            }
        });
      }
    } else {
      var id = $(this).attr("data-id");
      var getText = $('#'+id).html();
      var decoded = getText.replace(/&amp;/g, '&');
      $("#editcatname").val(decoded);
      $("#exampleModal-2").modal('show');
      // alert(name);
      $("#editCat").click(function(event) {
        event.preventDefault();
        update_data(id, 'name', $('#editcatname').val());        
  }) 
    }
  });
  

  function update_data(id, column_name, value) {
    $.ajax({
      url:"actionFunction/category/updateCategory.php",
      method:"POST",
      data:{id:id, column_name:column_name, value:value},
      success:function(data) {
        //$('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
        $('#category_data').DataTable().destroy();
        fetch_data();
        $("#exampleModal-2").modal('hide');
      }
    });
  }  

  function fetch_data() {
    var dataTable = $('#category_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"actionFunction/category/fatchCategory.php",
     type:"POST",
     datatype:"json"
    },
    success:function(data) {
      alert(data);
    }
   });
  }
})
</script>
</body>

</html>