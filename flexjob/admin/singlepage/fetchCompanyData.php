<?php

	function get_company_data($con,$uid) {
		$query = "SELECT * From company_tbl WHERE id=$uid";
		$result = mysqli_query($con, $query);
		$arr = array();
		if (mysqli_num_rows($result) > 0) {
   			 while($row = mysqli_fetch_assoc($result)) {
        		$arr[] = $row;
    		}
		} else {
    		echo "0 results";
		}
		return $arr;
	}

	function get_job_data($con,$uid){
		$query = "SELECT * From job_tbl WHERE c_id=$uid";
		$result = mysqli_query($con, $query);
		$arr = array();
		while($row = mysql_fetch_assoc($result)){
			$array[] = $row;
		}
		return $arr;
	}

	function get_company_id($con,$uid){
		$query = "SELECT c_id From companyreport_tbl WHERE id=$uid";
		$result = mysqli_query($con, $query);
		$jid = "";
		while ($row = mysqli_fetch_row($result)) {
			$jid = $row[0];
		}
		return $jid;
	}

	function get_job_dataHTML($con,$uid){
		$query = "SELECT * From job_tbl WHERE c_id=$uid";
		$result = mysqli_query($con, $query);

		if(!$result) {
			echo '
				<div class="wt-experiencelisting wt-bgcolor">
					<div class="wt-title">
						<h3>No Data Fetch Somthing wrong</h3>
					</div>
				</div>
			';
			exit();
		}
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)){
				$data =  '
					<div class="wt-userlistinghold wt-featured wt-userlistingholdvtwo">
						<div class="wt-userlistingcontent">
							<div class="wt-contenthead">
								<div class="wt-title">
									<h2>'.$row["name"].'</h2>
								</div>
								<div class="wt-description">
									<p>'.$row["description"].'<p>
								</div>
								<div class="wt-tag wt-widgettag">';
								$skills = explode(',',$row["skills"]);
								foreach ($skills as $value) {
									$data = $data.'<a>'.$value .'</a>';
								}		
				$data = $data.'</div>
							</div>
							<div class="wt-viewjobholder">
								<ul>
									<li><span><i class="far fa-folder wt-viewjobfolder"></i>Type: '. $row["type"].'</span></li>
									<li><span><i class="far fa-clock wt-viewjobclock"></i>Duration: '. $row["duration"].'</span></li>
									<li><span><i class="far  fa fa-dollar wt-viewjobclock"></i>Budget: '.$row["price"].'</span></li>
									<li><span><i class="far wt-viewjobclock"></i>Status:
									';
							if ($row["status"]==0) {
								$data = $data.'Ready to apply';
							} else if ($row["status"]==1) {
								$data = $data.'Ongoing';
							} else if ($row["status"]==2) {
								$data = $data.'Completed';
							} else {
								$data = $data.'Cancle';
							}
							$data = $data.'				
									</span></li>
									<li class="wt-btnarea"><a href="jobsingle.php?jid='.$row["jid"].'" class="wt-btn">View Job</a></li>
								</ul>
							</div>
						</div>
					</div>
				';
				echo $data;
			}
		} else {
			echo '
				<div class="wt-experiencelisting wt-bgcolor">
					<div class="wt-title">
						<h3>No Data</h3>
					</div>
				</div>
			';
		}
	}

													
													

	function get_education_data($con,$uid){
		$query = "SELECT * From education_tbl WHERE uid=$uid";
		$result = mysqli_query($con, $query);
		$arr = array();
		while($row = mysql_fetch_assoc($result)){
			$array[] = $row;
		}
		return $arr;
	}

	function get_education_dataHTML($con,$uid){
		$query = "SELECT * From education_tbl WHERE uid=$uid";
		$result = mysqli_query($con, $query);
		
		if(!$result) {
			echo '
				<div class="wt-experiencelisting wt-bgcolor">
					<div class="wt-title">
						<h3>No Data Fetch Somthing wrong</h3>
					</div>
				</div>
			';
			exit();
		}
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)){
				echo '
					<div class="wt-experiencelisting wt-bgcolor">
						<div class="wt-title">
							<h3>'.$row['course'].'</h3>
						</div>
						<div class="wt-experiencecontent">
							<ul class="wt-userlisting-breadcrumb">
								<li><span><i class="far fa-building"></i> '. $row["university"].'</span></li>
								<li><span><i class="far fa-calendar"></i> '.$row["p_year"].'</span></li>
							</ul>
							<div class="wt-description">
								<p>'.$row["description"].'</p>
							</div>
						</div>
					</div>
				';
			}
		} else {
			echo '
				<div class="wt-experiencelisting wt-bgcolor">
					<div class="wt-title">
						<h3>No Data</h3>
					</div>
				</div>
			';
		}	
	}

	function check_verify($verify){
		if ($verify == 1) {
			echo '<a href="#"><i class="fa fa-check-circle"></i> Verified Company</a>';
		}
	}

	function selectproejcts($con,$id) {
		$p = array();
        $que="SELECT COUNT(*) as o from ongoing_tbl where u_id='$id'";
       $result= mysqli_query($con,$que);
       while($row = mysqli_fetch_assoc($result)) {
        	$p['0'] = $row["o"];
    	}
       $que="SELECT COUNT(*) as o from complete_tbl where u_id='$id'";
       $result= mysqli_query($con,$que);
       while($row = mysqli_fetch_assoc($result)) {
        	$p['1'] = $row["o"];
    	}      
       $que="SELECT COUNT(*) as o from cancel_tbl where u_id='$id'";
       $result= mysqli_query($con,$que);
       while($row = mysqli_fetch_assoc($result)) {
        	$p['2'] = $row["o"];
    	}
       
       return $p;
    }
?>