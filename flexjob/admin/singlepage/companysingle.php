<?php

  session_start();
  if (!isset($_SESSION["islogin"])) {
    header('Location: login.php');
  }

?>
<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Company Profile</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="/flexjob/apple-touch-icon.png">
	<link rel="icon" href="/flexjob/images/fox.png" type="image/x-icon">
	<link rel="stylesheet" href="/flexjob/dist/assets/bootstrap.min.css">
	<link rel="stylesheet" href="/flexjob/css/bootstrap.min.css">
	<link rel="stylesheet" href="/flexjob/css/normalize.css">
	<link rel="stylesheet" href="/flexjob/css/scrollbar.css">
	<link rel="stylesheet" href="/flexjob/css/fontawesome/fontawesome-all.css">
	<link rel="stylesheet" href="/flexjob/css/font-awesome.min.css">
	<link rel="stylesheet" href="/flexjob/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/flexjob/css/jquery-ui.css">
	<link rel="stylesheet" href="/flexjob/css/linearicons.css">
	<link rel="stylesheet" href="/flexjob/css/tipso.css">
	<link rel="stylesheet" href="/flexjob/css/chosen.css">
	<link rel="stylesheet" href="/flexjob/css/prettyPhoto.css">
	<link rel="stylesheet" href="/flexjob/css/main.css">
	<link rel="stylesheet" href="/flexjob/css/color.css">
	<link rel="stylesheet" href="/flexjob/css/transitions.css">
	<link rel="stylesheet" href="/flexjob/css/responsive.css">
	<script src="/flexjob/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
	<?php 
	require_once('../db.php');
	include('fetchCompanyData.php');
	$cid=$_GET['id'];

	if (isset($_GET['report'])) {
		$cid = get_company_id($con,$cid);
	}
	

	$compdata =get_company_data($con,$cid);
	$imgLocation = '';
	if($compdata[0]["img"]=="" || $compdata[0]["img"]=="NULL") {
		$imgLocation="/flexjob/images/upload/1585820076.jpg";
	}
	else {
		$imgLocation="/flexjob/images/upload/".$compdata[0]["img"];
	}
	?>
<style>
	.backphoto{
		max-width:1200px;
		max-height:300px;
	}
</style>
</head>
<body class="wt-login">

	<!-- Wrapper Start -->
	<div id="wt-wrapper" class="wt-wrapper wt-haslayout">
		<!-- Content Wrapper Start -->
		<div class="wt-contentwrapper">
			<!-- Header Start -->
			
			<!--Header End-->
			<!--Inner Home Banner Start-->
			<div class="wt-haslayout wt-innerbannerholder">
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-xs-12 col-sm-12 col-md-8 push-md-2 col-lg-6 push-lg-3">
							<div class="wt-innerbannercontent">
							<div class="wt-title"><h2>Company Detail</h2></div>
							<ol class="wt-breadcrumb">
								<li><a href="#" onclick="window.history.back();">Back to Site</a></li>
							</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--Inner Home End-->
			<!--Main Start-->
			<main id="wt-main" class="wt-main wt-haslayout wt-innerbgcolor">
				<div class="wt-haslayout wt-main-section">
					<!-- User Listing Start-->
					<div class="wt-haslayout">
						<div class="container">
							<div class="row">
								<div id="wt-twocolumns" class="wt-twocolumns wt-haslayout">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 float-left">
										<div class="wt-comsingleimg">
											<figure><img src="<?php echo $imgLocation;?>" alt="img description" class="backphoto" /></figure>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-4 float-left">
										<aside id="wt-sidebar" class="wt-sidebar">
											<div class="wt-proposalsr wt-proposalsrvtwo">
												<div class="wt-widgetcontent wt-companysinfo">
													<div style="margin:80px;"></div>
													<div class="wt-title">
														<?php check_verify($compdata[0]["approve"]);?>
														<h2><?php echo $compdata[0]["name"];?></h2>
													</div>	
												</div>
											</div>
										</aside>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xl-8 float-left">
										<div class="wt-userlistingholder wt-haslayout">
											<div class="wt-comcontent">
												<div class="wt-title">
													<h3>About “<?php echo $compdata[0]["name"];?>”</h3>
												</div>
												<div class="wt-description">
													<p><?php echo $compdata[0]["description"];?></p>
												</div>
											</div>
											<?php get_job_dataHTML($con,$cid);?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- User Listing End-->
				</div>
			</main>
			<!--Main End-->
			
		</div>
		<!--Content Wrapper End-->
	</div>
	<!--Wrapper End-->
	<script src="/flexjob/js/vendor/jquery-3.3.1.js"></script>
	<script src="/flexjob/js/vendor/jquery-library.js"></script>
	<script src="/flexjob/js/vendor/bootstrap.min.js"></script>
	<script src="/flexjob/js/owl.carousel.min.js"></script>
	<script src="/flexjob/js/chosen.jquery.js"></script>
	<script src="/flexjob/js/tilt.jquery.js"></script>
	<script src="/flexjob/js/scrollbar.min.js"></script>
	<script src="/flexjob/js/prettyPhoto.js"></script>
	<script src="/flexjob/js/jquery-ui.js"></script>
	<script src="/flexjob/js/readmore.js"></script>
	<script src="/flexjob/js/countTo.js"></script>
	<script src="/flexjob/js/appear.js"></script>
	<script src="/flexjob/js/tipso.js"></script>
	<script src="/flexjob/js/jRate.js"></script>
	<script src="/flexjob/js/main.js"></script>
</body>

</html>