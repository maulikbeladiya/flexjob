<?php

	function get_user_data($con,$uid) {
		$query = "SELECT * From user_tbl WHERE uid=$uid";
		$result = mysqli_query($con, $query);
		$arr = array();
		if (mysqli_num_rows($result) > 0) {
   			 while($row = mysqli_fetch_assoc($result)) {
        		$arr[] = $row;
    		}
		} else {
    		echo "0 results";
		}
		return $arr;
	}

	function get_experiance_data($con,$uid){
		$query = "SELECT * From experiance_tbl WHERE uid=$uid";
		$result = mysqli_query($con, $query);
		$arr = array();
		while($row = mysql_fetch_assoc($result)){
			$array[] = $row;
		}
		return $arr;
	}

	function get_user_id($con,$uid){
		$query = "SELECT u_id From userreport_tbl WHERE id=$uid";
		$result = mysqli_query($con, $query);
		$jid = "";
		while ($row = mysqli_fetch_row($result)) {
			$jid = $row[0];
		}
		return $jid;
	}

	function get_experiance_dataHTML($con,$uid){
		$query = "SELECT * From experience_tbl WHERE uid=$uid";
		$result = mysqli_query($con, $query);

		if(!$result) {
			echo '
				<div class="wt-experiencelisting wt-bgcolor">
					<div class="wt-title">
						<h3>No Data Fetch Somthing wrong</h3>
					</div>
				</div>
			';
			exit();
		}
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)){
				echo '
					<div class="wt-experiencelisting wt-bgcolor">
						<div class="wt-title">
							<h3>'.$row['post'].'</h3>
						</div>
						<div class="wt-experiencecontent">
							<ul class="wt-userlisting-breadcrumb">
								<li><span><i class="far fa-building"></i> '. $row["c_name"].'</span></li>
								<li><span><i class="far fa-calendar"></i> '.$row["s_date"].' - '.$row["l_date"].'</span></li>
							</ul>
							<div class="wt-description">
								<p>'.$row["description"].'</p>
							</div>
						</div>
					</div>
				';
			}
		} else {
			echo '
				<div class="wt-experiencelisting wt-bgcolor">
					<div class="wt-title">
						<h3>No Data</h3>
					</div>
				</div>
			';
		}
	}
													

	function get_education_data($con,$uid){
		$query = "SELECT * From education_tbl WHERE uid=$uid";
		$result = mysqli_query($con, $query);
		$arr = array();
		while($row = mysql_fetch_assoc($result)){
			$array[] = $row;
		}
		return $arr;
	}

	function get_education_dataHTML($con,$uid){
		$query = "SELECT * From education_tbl WHERE uid=$uid";
		$result = mysqli_query($con, $query);
		
		if(!$result) {
			echo '
				<div class="wt-experiencelisting wt-bgcolor">
					<div class="wt-title">
						<h3>No Data Fetch Somthing wrong</h3>
					</div>
				</div>
			';
			exit();
		}
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)){
				echo '
					<div class="wt-experiencelisting wt-bgcolor">
						<div class="wt-title">
							<h3>'.$row['course'].'</h3>
						</div>
						<div class="wt-experiencecontent">
							<ul class="wt-userlisting-breadcrumb">
								<li><span><i class="far fa-building"></i> '. $row["university"].'</span></li>
								<li><span><i class="far fa-calendar"></i> '.$row["p_year"].'</span></li>
							</ul>
							<div class="wt-description">
								<p>'.$row["description"].'</p>
							</div>
						</div>
					</div>
				';
			}
		} else {
			echo '
				<div class="wt-experiencelisting wt-bgcolor">
					<div class="wt-title">
						<h3>No Data</h3>
					</div>
				</div>
			';
		}	
	}


	function selectproejcts($con,$id) {
		$p = array();
        $que="SELECT COUNT(*) as o from ongoing_tbl where u_id='$id'";
       $result= mysqli_query($con,$que);
       while($row = mysqli_fetch_assoc($result)) {
        	$p['0'] = $row["o"];
    	}
       $que="SELECT COUNT(*) as o from complete_tbl where u_id='$id'";
       $result= mysqli_query($con,$que);
       while($row = mysqli_fetch_assoc($result)) {
        	$p['1'] = $row["o"];
    	}      
       $que="SELECT COUNT(*) as o from cancel_tbl where u_id='$id'";
       $result= mysqli_query($con,$que);
       while($row = mysqli_fetch_assoc($result)) {
        	$p['2'] = $row["o"];
    	}
       
       return $p;
    }
?>