<?php
	
	function get_job_data($con,$uid) {
		$query = "SELECT * From job_tbl WHERE jid=$uid";
		$result = mysqli_query($con, $query);
		if (mysqli_num_rows($result) > 0) {
   			 while($row = mysqli_fetch_assoc($result)) {
        		return $row;
    		}
		} else {
    		
    		return;
		}
	}

	function get_company_data($con,$uid) {
		$query = "SELECT * From company_tbl WHERE id=$uid";
		$result = mysqli_query($con, $query);
		if (mysqli_num_rows($result) > 0) {
   			 while($row = mysqli_fetch_assoc($result)) {
        		return $row;
    		}
		} else {
    		echo "0 results";
    		return ;
		}
	}

	function get_job_id($con,$jid,$jobcat){
		$query = "";
		if ($jobcat == 1) {
			$query = "SELECT j_id From ongoing_tbl WHERE id=$jid";	
		} else if ($jobcat == 2) {
			$query = "SELECT j_id From complete_tbl WHERE id=$jid";
		} else if ($jobcat == 3) {
			$query = "SELECT j_id From cancel_tbl WHERE id=$jid";
		} else if ($jobcat == 4) {
			$query = "SELECT j_id From jobreport_tbl WHERE id=$jid";
		}
		$result = mysqli_query($con, $query);
		$jid = "";
		while ($row = mysqli_fetch_row($result)) {
			$jid = $row[0];
		}
		return $jid;
	}

	function get_freelancer_data($con,$fid,$tblName) {
		$query = "SELECT img,name,approve From user_tbl WHERE uid = (SELECT u_id From $tblName WHERE j_id = $fid)";
		$result = mysqli_query($con, $query);
		while ($row = mysqli_fetch_row($result)) {
			return $row;
		}
	}

	function check_status($status) {
		$content = '<li><span><i class="far fa-clock"></i> Status: ';
		if ($status == 0) {
			$content = $content.'Pending';
		} else if ($status == 1) {
			$content = $content.'Ongoing';
		} else if ($status == 2) {
			$content = $content.'Completed';
		} else if ($status == 3) {
			$content = $content.'Cancel';
		} else {
			$content = $content.'Somthing Wrong';
		}
		$content = $content.'</span></li>';
		echo $content;
	}



?>