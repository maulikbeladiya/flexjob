<?php

  session_start();
  if (!isset($_SESSION["islogin"])) {
    header('Location: login.php');
  }

?>
<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Job single</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="/flexjob/apple-touch-icon.png">
	<link rel="icon" href="/flexjob/images/fox.png" type="image/x-icon">
	<link rel="stylesheet" href="/flexjob/css/bootstrap.min.css">
	<link rel="stylesheet" href="/flexjob/css/normalize.css">
	<link rel="stylesheet" href="/flexjob/css/scrollbar.css">
	<link rel="stylesheet" href="/flexjob/css/fontawesome/fontawesome-all.css">
	<link rel="stylesheet" href="/flexjob/css/font-awesome.min.css">
	<link rel="stylesheet" href="/flexjob/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/flexjob/css/jquery-ui.css">
	<link rel="stylesheet" href="/flexjob/css/linearicons.css">
	<link rel="stylesheet" href="/flexjob/css/tipso.css">
	<link rel="stylesheet" href="/flexjob/css/chosen.css">
	<link rel="stylesheet" href="/flexjob/css/prettyPhoto.css">
	<link rel="stylesheet" href="/flexjob/css/main.css">
	<link rel="stylesheet" href="/flexjob/css/color.css">
	<link rel="stylesheet" href="/flexjob/css/transitions.css">
	<link rel="stylesheet" href="/flexjob/css/responsive.css">
	<link rel="stylesheet" href="/flexjob/css/dbresponsive.css">
	<link rel="stylesheet" href="/flexjob/css/dashboard.css">
	<script src="/flexjob/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
	<?php 
	require_once '../db.php';
	include 'fetchJobData.php';
	$jid = $_GET['jid']; //mysqli_real_escape_string($con, $_POST["jid"]);//
	$userdata = array();
	if (isset($_GET['redirectjob'])) {
		$jobcat = $_GET['jobcat'];
		if ($jobcat == 1) {
			$userdata=get_freelancer_data($con,$jid,'ongoing_tbl');
		} else if ($jobcat == 2) {
			$userdata=get_freelancer_data($con,$jid,'complete_tbl');
		}
	} else if (isset($_GET['jobcat']) && $_GET['jobcat']!=0 && $_GET['jobcat']!=3) {
		$jobcat = $_GET['jobcat'];
		$jid = get_job_id($con,$jid,$jobcat);
		if ($jobcat == 1) {
			$userdata=get_freelancer_data($con,$jid,'ongoing_tbl');
		} else if ($jobcat == 2) {
			$userdata=get_freelancer_data($con,$jid,'complete_tbl');
		}
	}
	
	$jobdata=get_job_data($con,$jid);
	$compdata=get_company_data($con,$jobdata['c_id']);
	?>

</head>
<body class="wt-login">

	<!-- Wrapper Start -->
	<div id="wt-wrapper" class="wt-wrapper wt-haslayout">
		<!-- Content Wrapper Start -->
		<div class="wt-contentwrapper">

			<!--Inner Home Banner Start-->
			<div class="wt-haslayout wt-innerbannerholder">
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-xs-12 col-sm-12 col-md-8 push-md-2 col-lg-6 push-lg-3">
							<div class="wt-innerbannercontent">
							<div class="wt-title"><h2>Job Detail</h2></div>
							 <ol class="wt-breadcrumb">
								<li><a href="#" onclick="window.history.back();">Back to Site</a></li>
							</ol> 
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--Inner Home End-->
			<!--Main Start-->
			<main id="wt-main" class="wt-main wt-haslayout wt-innerbgcolor">
				<div class="wt-haslayout wt-main-section">
					<!-- User Listing Start-->
					
					<div class="container">
						<div class="row">
							<div id="wt-twocolumns" class="wt-twocolumns wt-haslayout">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 float-left">
									<div class="wt-proposalholder">
										<div class="wt-proposalhead">
											<h2><?php echo $jobdata["name"];?></h2>
											<ul class="wt-userlisting-breadcrumb wt-userlisting-breadcrumbvtwo">
												<li><span><i class="far fa-folder"></i> Type: <?php echo $jobdata["categories"];?></span></li>
												<li><span><i class="far fa-clock"></i> Duration: <?php echo $jobdata["duration"];?></span></li>
												<li><span><i class="far fa-clock"></i> Budget: <?php echo $jobdata["price"];?>$</span></li>
												<?php  check_status($jobdata['status']);?>
											</ul>
										</div>
									
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xl-8 float-left">
									<div class="wt-projectdetail-holder">
										<div class="wt-projectdetail">
											<div class="wt-tabscontenttitle">
												<h2>Project Details</h2>
											</div>
											<div class="wt-description wt-jobdetailscontent">
												<p><?php echo $jobdata["description"];?></p>
											</div>
										</div>
										<div class="wt-skillsrequired">
											<div class="wt-tabscontenttitle">
												<h2>Skills Required</h2>
											</div>
											<div class="wt-tag wt-widgettag wt-jobdetailscontent">
											<?php  
											$skills = explode(',',$jobdata["skills"]);
											foreach ($skills as $value) {
												echo '<a>'.$value.'</a>';	
											}
											?>	
											</div>
										</div>
									</div>
								</div>
							
								<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-4 float-left">
									<aside id="wt-sidebar" class="wt-sidebar">
										<div class="wt-widget wt-companysinfo-jobsingle">
											<div class="wt-companysdetails">
												<figure class="wt-companysimg">
													<img src="/flexjob/images/upload/<?php echo $compdata["img"];?>" alt="img description">
												</figure>
												<div class="wt-companysinfo">
													<div style="margin:75px;"></div>
													<div class="wt-title">
													<?php 
														if($compdata["approve"]==1) {
															echo '<a href="javascript:void(0);"><i class="fa fa-check-circle"></i> Verified Company</a>';
														}
														echo '<h2>'.$compdata["name"].'</h2>';
													?>
													</div>
													
												</div>
											</div>
										</div>
										<?php
											if (isset($_GET['jobcat'])) {
												$jobcat = $_GET['jobcat'];
												if ($jobcat != 0 && $jobcat != 3 && $jobcat != 4) {
													echo '
														<div class="wt-widget wt-companysinfo-jobsingle">
															<div class="wt-companysdetails">
																<figure class="wt-companysimg">
																	<img src="/flexjob/images/upload/'.$userdata[0].'" alt="img description">
															</figure>
															<div class="wt-companysinfo">
																<div style="margin:75px;"></div>
																	<div class="wt-title">' ;
																	if($userdata[2]==1) {
																		echo '<a href="javascript:void(0);"><i class="fa fa-check-circle"></i> Verified User</a>';
																	}
																echo '<h2>'.$userdata[1].'</h2>
																</div>
															</div>
														</div>
													</div>';
												}
											}
											
										?>
									</aside>
								</div>
							</div>
						</div>
					</div>
					<!-- User Listing End-->
				</div>
			</main>
			<!--Main End-->
		</div>
		<!--Content Wrapper End-->
	</div>
	<!--Wrapper End-->
	<script src="/flexjob/js/vendor/jquery-3.3.1.js"></script>
	<script src="/flexjob/js/vendor/jquery-library.js"></script>
	<script src="/flexjob/js/vendor/bootstrap.min.js"></script>
	<script src="/flexjob/js/owl.carousel.min.js"></script>
	<script src="/flexjob/js/chosen.jquery.js"></script>
	<script src="/flexjob/js/tilt.jquery.js"></script>
	<script src="/flexjob/js/scrollbar.min.js"></script>
	<script src="/flexjob/js/prettyPhoto.js"></script>
	<script src="/flexjob/js/jquery-ui.js"></script>
	<script src="/flexjob/js/readmore.js"></script>
	<script src="/flexjob/js/countTo.js"></script>
	<script src="/flexjob/js/appear.js"></script>
	<script src="/flexjob/js/tipso.js"></script>
	<script src="/flexjob/js/jRate.js"></script>
	<script src="/flexjob/js/main.js"></script>
</body>


</html>