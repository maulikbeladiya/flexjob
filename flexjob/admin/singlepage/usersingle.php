<?php

  session_start();
  if (!isset($_SESSION["islogin"])) {
    header('Location: login.php');
  }

?>
<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Users</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="/flexjob/apple-touch-icon.png">
	<link rel="icon" href="/flexjob/images/fox.png" type="image/x-icon">
	<link rel="stylesheet" href="/flexjob/css/bootstrap.min.css">
	<link rel="stylesheet" href="/flexjob/css/normalize.css">
	<link rel="stylesheet" href="/flexjob/css/scrollbar.css">
	<link rel="stylesheet" href="/flexjob/css/fontawesome/fontawesome-all.css">
	<link rel="stylesheet" href="/flexjob/css/font-awesome.min.css">
	<link rel="stylesheet" href="/flexjob/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/flexjob/css/jquery-ui.css">
	<link rel="stylesheet" href="/flexjob/css/linearicons.css">
	<link rel="stylesheet" href="/flexjob/css/tipso.css">
	<link rel="stylesheet" href="/flexjob/css/chosen.css">
	<link rel="stylesheet" href="/flexjob/css/prettyPhoto.css">
	<link rel="stylesheet" href="/flexjob/css/main.css">
	<link rel="stylesheet" href="/flexjob/css/color.css">
	<link rel="stylesheet" href="/flexjob/css/transitions.css">
	<link rel="stylesheet" href="/flexjob/css/responsive.css">
	<script src="/flexjob/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


	<?php 
	include('../db.php');
	include('fetchUserData.php');
	$userId = $_GET['id'];
	if (isset($_GET['report'])) {
		$userId = get_user_id($con,$userId);
	}

	$userdata=get_user_data($con,$userId);
	// $experiancedata=get_experiance_data($con,91);
	// $educationdata=get_education_data($con,91);
	$projects = selectproejcts($con,$userId);
	$imgLocation = '';
	if($userdata[0]["img"]=="" || $userdata[0]["img"]=="NULL") {
		$imgLocation="/flexjob/images/upload/default.jpg";
	}
	else {
		$imgLocation="/flexjob/images/upload/".$userdata[0]["img"];
	}
	?> 
</head>
<body class="wt-login">

	<!-- Preloader Start -->
	<!-- <div class="preloader-outer">
		<div class="loader"></div>
	</div>
	 --><!-- Preloader End -->
	<!-- Wrapper Start -->
	<div id="wt-wrapper" class="wt-wrapper wt-haslayout">
		<!-- Content Wrapper Start -->
		<div class="wt-contentwrapper">
			
			<!--Inner Home Banner Start-->
			<div class="wt-haslayout wt-innerbannerholder wt-innerbannerholdervtwo">
				<div class="container">
					<div class="row justify-content-md-center">
 						<div class="col-xs-12 col-sm-12 col-md-8 push-md-2 col-lg-6 push-lg-3">
							<div class="wt-innerbannercontent">
								<div class="wt-title"><h2>Freelancer Detail</h2></div>
							 		<ol class="wt-breadcrumb">
										<li><a href="#" onclick="window.history.back();">Back to Site</a></li>
									</ol> 
							
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--Inner Home End-->
			<!--Main Start-->
			<main id="wt-main" class="wt-main wt-haslayout wt-innerbgcolor">
				<!-- User Profile Start-->
				<div class="wt-main-section wt-paddingtopnull wt-haslayout">
					<div class="container">
						<div class="row">	
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 float-left">
								<div class="wt-userprofileholder">
									<div class="col-12 col-sm-12 col-md-12 col-lg-3 float-left">
										<div class="row">
											<div class="wt-userprofile">
							
												<figure>
												<img src="<?php echo $imgLocation;?>" alt="image description" style="margin-left:45px; vertical-align: middle; width:165px; height:165px; border-radius: 5%;">
												</figure>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-12 col-md-12 col-lg-9 float-left">
										<div class="row">
											<div class="wt-proposalhead wt-userdetails">
											<h2> <?php  if($userdata[0]["approve"] == 1){
													echo '<i class="fa fa-check-circle success"></i> ';
											}
											echo $userdata[0]["name"];
											?>
											</h2>
												<ul class="wt-userlisting-breadcrumb wt-userlisting-breadcrumbvtwo">
													<li><span><i class="far fa-money-bill-alt"></i> <?php echo "$".$userdata[0]["price"]; ?>/ hr</span></li>
													<!-- <li><a href="javascript:void(0);" class="wt-clicksave"><i class="fa fa-heart"></i> Save</a></li> -->
												</ul>
												<div class="wt-description">
													<p><?php echo $userdata[0]["description"]; ?></p>
												</div>
											</div>
											<div id="wt-statistics" class="wt-statistics wt-profilecounter">
												<div class="wt-statisticcontent wt-countercolor1">
													<h3 data-from="0" data-to="<?php echo $projects[0]; ?>" data-speed="800" data-refresh-interval="03">03</h3>
													<h4>Ongoing <br>Projects</h4>
												</div>
												<div class="wt-statisticcontent wt-countercolor2">
													<h3 data-from="0" data-to="<?php echo $projects[1]; ?>" data-speed="800" data-refresh-interval="02">1503</h3>
													<h4>Completed <br>Projects</h4>
												</div>
												<div class="wt-statisticcontent wt-countercolor4">
													<h3 data-from="0" data-to="<?php echo $projects[2]; ?>" data-speed="800" data-refresh-interval="02">02</h3>
													<h4>Cancelled <br>Projects</h4>
												</div>
											
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- User Profile End-->
					<!-- User Listing Start-->
					<div class="container">
						<div class="row">
							<div id="wt-twocolumns" class="wt-twocolumns wt-haslayout">
								<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xl-8 float-left">
									<div class="wt-usersingle">
										<div class="wt-experience">
											<div class="wt-usertitle">
												<h2>Experience</h2>
											</div>
											<div class="wt-experiencelisting-hold">
											<?php  get_experiance_dataHTML($con,$userId);?>	
												<div class="divheight"></div>
											</div>
										</div>
										<div class="wt-experience wt-education">
											<div class="wt-usertitle">
												<h2>Education</h2>
											</div>
											<div class="wt-experiencelisting-hold">
											<?php get_education_dataHTML($con,$userId);?>
												<div class="divheight"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-4 float-left">
									<aside id="wt-sidebar" class="wt-sidebar">
										<div id="wt-ourskill" class="wt-widget">
											<div class="wt-widgettitle">
												<h2>My Skills</h2>
											</div>
											<div class="wt-widgetcontent wt-skillscontent">
												<div class="wt-skillholder" data-percent="50%">
													<span> <?php 
														$str_arr = explode (",", $userdata[0]["skills"]);
														echo '<div class="wt-tag wt-widgettag">';
														foreach ($str_arr as $value) {
															echo "<a>$value </a>";
														}
														echo('</div');
													?> </span>
												</div>	
											</div>
										</div>									
									</aside>
								</div>
							</div>
						</div>
					</div>
					<!-- User Listing End-->
				</div>
			</main>
			<!--Main End-->
		</div>
		<!--Content Wrapper End-->
	</div>
	<!--Wrapper End-->
	
	<script src="/flexjob/js/vendor/jquery-3.3.1.js"></script>
	<script src="/flexjob/js/vendor/jquery-library.js"></script>
	<script src="/flexjob/js/vendor/bootstrap.min.js"></script>
	<script src="/flexjob/js/owl.carousel.min.js"></script>
	<script src="/flexjob/js/chosen.jquery.js"></script>
	<script src="/flexjob/js/tilt.jquery.js"></script>
	<script src="/flexjob/js/scrollbar.min.js"></script>
	<script src="/flexjob/js/prettyPhoto.js"></script>
	<script src="/flexjob/js/jquery-ui.js"></script>
	<script src="/flexjob/js/readmore.js"></script>
	<script src="/flexjob/js/countTo.js"></script>
	<script src="/flexjob/js/appear.js"></script>
	<script src="/flexjob/js/tipso.js"></script>
	<script src="/flexjob/js/jRate.js"></script>
	<script src="/flexjob/js/main.js"></script>
</body>


</html>