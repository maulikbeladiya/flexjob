<?php
  session_start();
  if (!isset($_SESSION["islogin"])) {
    header('Location: login.php');
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>FlexJob Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/jquery-toast-plugin/jquery.toast.min.css">
  <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/base/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  

  
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php require_once('partials/_navbar.php');?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

      <?php require_once('partials/_sidebar.php');?>


      <div class="main-panel">
        <div class="content-wrapper">
            <div class="col-md-12 stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">All Clients</p>
                  <div class="table-responsive">
                    <table id="client_data" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Id</th>
                            <th>User</th>
                            <th>Company Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <?php require_once 'partials/_footer.php';?>
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- <script src="vendors/chart.js/Chart.min.js"></script> -->
  <script src="vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <script src="vendors/jquery-toast-plugin/jquery.toast.min.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <script src="js/data-table.js"></script>
  <script src="js/jquery.dataTables.js"></script>
  <script src="js/dataTables.bootstrap4.js"></script>
  <!-- <script src="js/file-upload.js"></script> -->
  <!-- <script src="js/toastDemo.js"></script> -->
  <!-- End custom js for this page-->
  

<script type="text/javascript">

 $(document).ready(function(){
  
  // MARK: fetchData from database for datatabels
  showToastPosition = function(position) {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Positioning',
      text: 'Specify the custom position object or use one of the predefined ones',
      position: String(position),
      icon: 'success',
      stack: true,
      loaderBg: '#f96868'
    })
  }

  resetToastPosition = function() {
    $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
    $(".jq-toast-wrap").css({
      "top": "",
      "left": "",
      "bottom": "",
      "right": ""
    }); //to remove previous position style
  }

  fetch_data();


  $('#client_data tbody').on('click', 'button', function(){
    var name = $(this).attr("name");
    showToastPosition('top-right');
    if (name == "delete") {
      var id = $(this).attr("data-id");
      if(confirm("Are you sure you want to remove this?")) {
        $.ajax({
            url:"actionFunction/client/delete.php",
            method:"POST",
            data:{
              id:id,
              colid : 'id',
              tblName:"company_tbl"
            },
            success:function(data){
              $('#client_data').DataTable().destroy();
              fetch_data();
            }
        });
      }
    } else if (name == "aprove") {
      var id = $(this).attr("data-id");
      if(confirm("Are you sure you want to aprove this user?")) {
        $.ajax({
            url:"actionFunction/client/verify.php",
            method:"POST",
            data:{
              id:id,
              colid : 'id',
              column_name: "approve",
              value: 1,
              tblName: "company_tbl"
            },
            success:function(data){
              $('#client_data').DataTable().destroy();
              fetch_data();
            }
        });
      }
    } else if (name == "disaprove") {
      var id = $(this).attr("data-id");
      if(confirm("Are you sure you want to disaprove this user?")) {
        $.ajax({
            url:"actionFunction/client/verify.php",
            method:"POST",
            data:{
              id:id,
              colid : 'id',
              column_name: "approve",
              value: 0,
              tblName: "company_tbl"
            },
            success:function(data){
              $('#client_data').DataTable().destroy();
              fetch_data();
            }
        });
      }
    } else if (name == "view") {
      var id = $(this).attr("data-id");
      myWindow = window.location.href = 'http://localhost/flexjob/admin/singlepage/companysingle.php?id='+id; // myWindow = window.open('http://localhost/admin/singlepage/jobsingle.php?jid='+id, "_blank");
        myWindow.focus();
    }
  });  

  function fetch_data() {
    var dataTable = $('#client_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"actionFunction/client/fetch.php",
     type:"POST",
     data:{
        colName: "id",
        tblName: "company_tbl"  
     },
     datatype:"json"
    },
    success:function(data) {
      alert(data);
    }
   });
  }
})
</script>
</body>

</html>