<?php

require_once '../../db.php';

$columns = array('id','id',$_POST["colid"],'description','status','id');
$tblName = $_POST["tblName"];
$query = "SELECT * FROM ".$tblName;
if(isset($_POST["search"]["value"]))
{
 $query .= '
 WHERE '.$_POST["colid"].' LIKE "%'.$_POST["search"]["value"].'%" OR description LIKE "%'.$_POST["search"]["value"].'%"';
}
if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY id DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$number_filter_row = mysqli_num_rows(mysqli_query($con, $query));
$result = mysqli_query($con,$query . $query1);
$data = array();
$count = 1;
while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = "$count";
 $sub_array[] = $row["id"];
 $sub_array[] = $row[$_POST["colid"]];
 $sub_array[] = '<div style="white-space:normal; width:300px;">'.$row["description"].'</div>';
 if ($row['status'] == 0) {
 	$sub_array[] = '<center><label class="badge badge-danger">Pending</label></center>';
 	$sub_array[] = '<center><button class="btn btn-outline-warning mdi mdi-delete-variant" name="deletereport" id="deletereport" data-id="' . $row["id"] . '"></button> <button class="btn btn-outline-danger mdi mdi-delete-variant" name="deleterecord" id="deleterecord" data-id="' . $row["id"] . '"></button> <button class="btn btn-outline-info mdi mdi-account" name="view" id="view" data-id="' . $row["id"] . '"></button></center>';
 } else if ($row['status'] == 1) {
 	$sub_array[] = '<center><label class="badge badge-success">Completed</label></center>';
 	$sub_array[] = '<center><button class="btn btn-outline-danger mdi mdi-delete-variant" name="deleterecord" id="deleterecord" data-id="' . $row["id"] . '"></button> </center>';
 } else {
 	$sub_array[] = '<center><label class="badge badge-warning">on progress</label></center>';
 	$sub_array[] = '<center><button class="btn btn-outline-danger mdi mdi-delete-variant" name="deleterecord" id="deleterecord" data-id="' . $row["id"] . '"></button> </center>';
 }
 $data[] = $sub_array;
 $count += 1; 
}


function get_all_data($connect,$tblName) {
 $query = "SELECT * FROM $tblName";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

function get_client_info($con,$cid) {
	$query = "SELECT name,img From company_tbl WHERE id=$cid";
	$result = mysqli_query($con, $query);
	$arr = array();
	while ($row = mysqli_fetch_row($result)) {
		$arr[] = $row[0];
		$arr[] = $row[1];
		// return $arr;
	}	
	
	return $arr;
}


$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($con,$tblName),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);
close_con($con);
?>