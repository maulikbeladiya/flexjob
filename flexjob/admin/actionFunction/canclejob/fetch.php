<?php

require_once '../../db.php';
// require_once '../../utils.php';

$columns = array('id');
$tblName = "cancel_tbl";//$_POST["tblName"];
$query = "SELECT * FROM ".$tblName;
if(isset($_POST["search"]["value"]))
{
 $query .= '
 WHERE id LIKE "%'.$_POST["search"]["value"].'%"';
}
if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY id DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($con, $query));
$result = mysqli_query($con,$query . $query1);
$data = array();
$count = 1;
while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = $count;
 $sub_array[] = $row["id"];
 $sub_array[] = '<div style="white-space:normal; width:200px;">'.get_job_name($con,$row['j_id']).'</div>';
 //$tmpfreelance = get_freelancer_info($con,$row['fid']);
 $tmpclient = get_client_info($con,$row['c_id']);
 $sub_array[] =	'<center><div class="media"><img src="/flexjob/images/upload/'.$tmpclient[1].'" alt="image" /><div class="media-body"><h5>'.$tmpclient[0] .'</h5></div></div></center>';
 //$sub_array[] =	'<center><div class="media"><img src="images/faces/face'.$count.'.jpg" alt="image" /><div class="media-body"><h5>'.$tmpfreelance[0] .'</h5></div></div></center>';
 $sub_array[] = '<center><button type="button" class="btn btn-outline-info mdi mdi-account-box icon-sm d-flex align-self-center mr-3
" name="view" id="view" data-id="'.$row["id"].'"></button></center>';
 $data[] = $sub_array;
 $count += 1; 
}


function get_all_data($connect,$tblName) {
 $query = "SELECT * FROM $tblName";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

function get_job_name($con,$jid){
	$query = "SELECT name From job_tbl WHERE jid=$jid";
	$result = mysqli_query($con, $query);
	$name = "";
	while ($row = mysqli_fetch_row($result)) {
		$name = $row[0];
	}
	return $name;
}


function get_client_info($con,$cid) {
	$query = "SELECT name,img From company_tbl WHERE id=$cid";
	$result = mysqli_query($con, $query);
	$arr = array();
	while ($row = mysqli_fetch_row($result)) {
		$arr[] = $row[0];
		$arr[] = $row[1];
		// return $arr;
	}	
	
	return $arr;
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($con,$tblName),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

close_con($con);
?>