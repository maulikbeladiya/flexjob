<?php

require_once '../../db.php';


$tblName = $_POST["tblName"];
$colid = $_POST["colName"];
$columns = array($colid,$colid,$colid,'name','email','approve','approve');
$query = "SELECT * FROM ".$tblName;
if(isset($_POST["search"]["value"]))
{
 $query .= '
 WHERE name LIKE "%'.$_POST["search"]["value"].'%" OR email LIKE "%'.$_POST["search"]["value"].'%"';
}
if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY '.$colid.' DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$number_filter_row = mysqli_num_rows(mysqli_query($con, $query));
$result = mysqli_query($con,$query . $query1);
$data = array();
$count = 1;
while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = "$count";
 $sub_array[] = $row[$colid];
 $retVal = ($row['img'] == "Null" || $row['img'] == "") ? 'default.jpg' : $row['img'] ;
 $sub_array[] = '<center><img src="/flexjob/images/upload/'.$retVal.'" alt="image" /></center>';
 $sub_array[] = $row["name"];
 $sub_array[] = $row["email"];
 if ($row['approve'] == 0) {
 	$sub_array[] = '<center><label class="badge badge-danger">Decline</label></center>';
 	$sub_array[] = '<center><button class="btn btn-outline-danger mdi mdi-account-off" name="delete" id="delete" data-id="' . $row[$colid] . '"></button>	<button class="btn btn-outline-success mdi mdi-account-check" name="aprove" id="aprove" data-id="' . $row[$colid] . '"></button> <button class="btn btn-outline-info mdi mdi-account" name="view" id="view" data-id="' . $row[$colid] . '"></button></center>';
 } else if ($row['approve'] == 1) {
 	$sub_array[] = '<center><label class="badge badge-success">aproved</label></center>';
 	$sub_array[] = '<center><button class="btn btn-outline-danger mdi mdi-account-off" name="delete" id="delete" data-id="' . $row[$colid] . '"></button>	<button class="btn btn-outline-danger mdi mdi-account-remove" name="disaprove" id="disaprove" data-id="' . $row[$colid] . '"></button> <button class="btn btn-outline-info mdi mdi-account" name="view" id="view" data-id="' . $row[$colid] . '"></button></center>';
 } else {
 	$sub_array[] = '<center><label class="badge badge-warning">pending</label></center>';
 	$sub_array[] = '<center><button class="btn btn-outline-danger mdi mdi-account-off" name="delete" id="delete" data-id="' . $row[$colid] . '"></button> <button class="btn btn-outline-info mdi mdi-account" name="view" id="view" data-id="' . $row[$colid] . '"></button></center>';
 }
 $data[] = $sub_array;
 $count += 1; 
}


function get_all_data($connect,$tblName) {
 $query = "SELECT * FROM $tblName";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($con,$tblName),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

close_con($con);
?>