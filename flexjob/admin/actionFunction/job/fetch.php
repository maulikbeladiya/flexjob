<?php

require_once '../../db.php';
// require_once '../../utils.php';

$columns = array('jid','jid','date','name','categories','skills','duration','price','type','c_id','status','status');
$tblName = "job_tbl";//$_POST["tblName"];
$query = "SELECT * FROM ".$tblName;
if(isset($_POST["search"]["value"]))
{
 $query .= '
 WHERE jid LIKE "%'.$_POST["search"]["value"].'%"';
}
if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY jid DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($con, $query));
$result = mysqli_query($con,$query . $query1);
$data = array();
$count = 1;

while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = $count;
 $sub_array[] = $row["jid"];
 $sub_array[] = $row['date'];
 $sub_array[] = '<div style="white-space:normal; width:200px;">'.$row['name'].'</div>';
 $sub_array[] = $row['categories'];//get_category_name($con,$row['catid']);
 $sub_array[] = '<div style="white-space:normal; width:150px;">'.$row['skills'].'</div>';
 $sub_array[] = $row['duration'];
 $sub_array[] = '$ '.$row['price'];
 $sub_array[] = $row['type'];
 $tmpclient = get_client_info($con,$row['c_id']);
 $sub_array[] =	'<center><div class="media"><img src="/flexjob/images/upload/'.$tmpclient[1].'" alt="image" /><div class="media-body"><h5>'.$tmpclient[0] .'</h5></div></div></center>';
 if ($row['status'] == 0) {
 	$sub_array[] =	'<center><label class="badge badge-warning">Pending</label></center>';	
 } else if ($row['status'] == 1) {
 	$sub_array[] =	'<center><label class="badge badge-primary">Ongoing</label></center>';	
 }else if ($row['status'] == 2) {
 	$sub_array[] =	'<center><label class="badge badge-success">Completed</label></center>';	
 }if ($row['status'] == 3) {
 	$sub_array[] =	'<center><label class="badge badge-danger">Cancle</label></center>';	
 }
 
 
 $sub_array[] = '<center><button type="button" class="btn btn-outline-danger mdi mdi-delete icon-sm" name="delete" id="delete" data-id="'.$row["jid"].'"></button> <button type="button" jobstatus='.$row['status'].' class="btn btn-outline-info mdi mdi-account icon-sm" name="view" id="view" data-id="'.$row["jid"].'"></button></center>';
 $data[] = $sub_array;
 $count += 1; 
}


function get_all_data($connect,$tblName) {
 $query = "SELECT * FROM $tblName";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

function get_category_name($con,$catid) {
	$query = "SELECT name From category_tbl WHERE id=$catid";
	$result = mysqli_query($con, $query);
	$row = mysqli_fetch_assoc($result);
    return $row['name'];
}

function get_client_info($con,$cid) {
	$query = "SELECT name,img From company_tbl WHERE id=$cid";
	$result = mysqli_query($con, $query);
	$arr = array();
	while ($row = mysqli_fetch_row($result)) {
		$arr[] = $row[0];
		$arr[] = $row[1];
		// return $arr;
	}	
	
	return $arr;
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($con,$tblName),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

close_con($con);
?>