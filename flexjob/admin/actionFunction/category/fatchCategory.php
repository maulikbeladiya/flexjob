<?php

require_once '../../db.php';

$columns = array('id','id','name','id');

$query = "SELECT * FROM category_tbl ";
if(isset($_POST["search"]["value"]))
{
 $query .= '
 WHERE name LIKE "%'.$_POST["search"]["value"].'%"';
}
if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY id DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$number_filter_row = mysqli_num_rows(mysqli_query($con, $query));
$result = mysqli_query($con,$query . $query1);
$data = array();
$count = 1;
while($row = mysqli_fetch_array($result)) {
 $sub_array = array();
 $sub_array[] = $count;
 $sub_array[] = $row["id"];
 $sub_array[] = '<div class="update" id="'.$row["id"].'" data-id="'.$row["id"].'" data-column="name" style="white-space:normal; width:250px;">' . $row["name"] . '</div>';//$row["name"];
 $sub_array[] = '<center><button class="btn btn-outline-danger mdi mdi-delete" name="delete" id="delete" data-id="'.$row["id"].'"></button>	<button class="btn btn-outline-info mdi mdi-pencil
" name="update" id="update" data-id="'.$row["id"].'"></button></center>';
 $data[] = $sub_array;
 $count = $count + 1;
}

function get_all_data($connect)
{
 $query = "SELECT * FROM category_tbl";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($con),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

close_con($con);
?>