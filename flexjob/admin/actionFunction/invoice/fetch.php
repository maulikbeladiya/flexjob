<?php

require_once '../../db.php';
// require_once '../../utils.php';

$columns = array('oid','oid','date','item_name','item_number','u_oid','paid_amount','oid');
$tblName = "orders";//$_POST["tblName"];
$query = "SELECT * FROM ".$tblName;
if(isset($_POST["search"]["value"]))
{
 $query .= ' 
 WHERE oid LIKE "%'.$_POST["search"]["value"].'%"';
}
if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY oid DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($con, $query));
$result = mysqli_query($con,$query . $query1);
$data = array();
$count = 1;
while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = $count;
 $sub_array[] = $row["oid"];
 $sub_array[] = $row['date'];
 $sub_array[] = '<div style="white-space:normal; width:200px;">'.$row['item_name'].'</div>';
 $tmpfreelance = get_freelancer_info($con,$row['u_id']);
 $tmpclient = get_client_info($con,get_company_id($con,$row['item_number']));
 $sub_array[] =	'<center><div class="row align-items-center"><img src="/flexjob/images/upload/'.$tmpclient[1].'" alt="image" /><div class="ml-2"><h5>'.$tmpclient[0] .'</h5></div></div></center>';
 $sub_array[] =	'<center><div class="row align-items-center"><img src="/flexjob/images/upload/'.$tmpfreelance[1].'" alt="image" /><div class="ml-2"><h5>'.$tmpfreelance[0] .'</h5></div></div></center>';
 $sub_array[] = '$ '.$row['paid_amount'];
 $sub_array[] = '<center><button class="btn btn-outline-danger mdi mdi-delete" name="delete" id="delete" data-id="'.$row["oid"].'"></button>	<button class="btn btn-outline-info mdi mdi-account-box" name="view" id="view" data-id="'.$row["oid"].'"></button></center>';
 $data[] = $sub_array;
 $count += 1; 
}


function get_all_data($connect,$tblName) {
 $query = "SELECT * FROM $tblName";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

function get_company_id($con,$jid){
	$query = "SELECT c_id From job_tbl WHERE jid=$jid";
	$result = mysqli_query($con, $query);
	$id = "";
	while ($row = mysqli_fetch_row($result)) {
		$id = $row[0];
	}
	return $id;
}

function get_freelancer_info($con,$fid) {
	$query = "SELECT name,img From user_tbl WHERE uid=$fid";
	$result = mysqli_query($con, $query);
	$arr = array();
	while ($row = mysqli_fetch_row($result)) {
		$arr[] = $row[0];
		$arr[] = $row[1];
		// return $arr;
	}	
	return $arr;
}

function get_client_info($con,$cid) {
	$query = "SELECT name,img From company_tbl WHERE id=$cid";
	$result = mysqli_query($con, $query);
	$arr = array();
	while ($row = mysqli_fetch_row($result)) {
		$arr[] = $row[0];
		$arr[] = $row[1];
		// return $arr;
	}
	return $arr;
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($con,$tblName),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

close_con($con);
?>