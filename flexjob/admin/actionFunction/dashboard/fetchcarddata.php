<?php

function get_total_user($con) {
	return get_total_freelancer($con) + get_total_company($con); 
}

function get_total_freelancer($con){
    $query="SELECT * From user_tbl";
    $result = mysqli_query($con, $query);
    return mysqli_num_rows($result);
}

function get_total_company($con){
    $query="SELECT * From company_tbl";
    $result = mysqli_query($con, $query);
    return mysqli_num_rows($result);
}

function get_completed_job($con){
    $query="SELECT * From complete_tbl";
    $result = mysqli_query($con, $query);
    return mysqli_num_rows($result);
}

function get_ongoing_job($con){
    $query="SELECT * From ongoing_tbl";
    $result = mysqli_query($con, $query);
    return mysqli_num_rows($result);
}

function get_cancel_job($con){
    $query="SELECT * From cancel_tbl";
    $result = mysqli_query($con, $query);
    return mysqli_num_rows($result);
}

function get_total_job($con){
    $query="SELECT * From job_tbl";
    $result = mysqli_query($con, $query);
    return mysqli_num_rows($result);
}

?>