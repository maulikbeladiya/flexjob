<?php

require_once '../../db.php';

$query = "SELECT * FROM job_tbl ";
$query .= 'ORDER BY jid DESC ';
$query1 = 'LIMIT ' . 5;

$result = mysqli_query($con,$query . $query1);
$count = 1;
$tableData = "";
while($row = mysqli_fetch_array($result))
{
	$tableData =$tableData.'
		<tr>
			<td>'.$count.'</td>
            <td>'.$row['name'].'</td>
            <td>'.$row['date'].'</td>
            <td>$ '.$row['price'].'</td>';
            if ($row['status'] == 0) {
            	$tableData = $tableData.'<td><label class="badge badge-info">Pending</label></td><tr>';
            } else if ($row['status'] == 1) {
            	$tableData = $tableData.'<td><label class="badge badge-warning">Ongoing</label></td><tr>';
            } else if ($row['status'] == 2) {
            	$tableData = $tableData.'<td><label class="badge badge-success">Completed</label></td><tr>';
            } else if ($row['status'] == 3) {
            	$tableData = $tableData.'<td><label class="badge badge-danger">Cancel</label></td><tr>';
            }
            
 	$count += 1; 
}
echo $tableData;
close_con($con);
?>